
# flipbook.js #

flipbook.js is a jquery plugin which let you play a sequence of images as a small film on the fly.

## License #
Licensed under the MIT license.


## Options ##

### width ###
Width of the film

### height ###
Height of the film

### urls ###
An url of image array which represents each frames of the film.

### autoplay ###
If set to true, the animation will be played immediately when it is ready (all frames have been loaded). Default is false. 

### backward ###
If set to true, the animation wiil be played backwrad.

### fps ###
How many frames will be flipped in a seconds. Default is 30.

### repeat ###
If set to true, the animation will repeatly play.

## Events ##
### onPreload ###
Event triggered every frame have been loaded

### onLoad ###
Event triggered when all frames have been loaded

### onFrame ###
Event triggered when a frames have been drawed on screen.

### onFinish ###
Event triggered when all frames have been played.

## Mathods ##

### init() ###
Start to preload all images.

### play() ###
Start to play an animation. Requires all frames of image has been loaded.

### pause() ###
Pause the animation.

### stop() ###
Stop the animation. CurrentIndex will be reset.

### setCurrentIndex() ###
Set the current index. The status will be changed to 'pause'. 

## Properties ##
### isReady ###
Indicates whether all frames of image has been loaded.

### frames ###
An array of all frames of image and url.

### currentIndex ###
The index of current frame.

## HTML markup
After initialize the html markup will be beload.

    <div id="flipbook">
        <canvas id="" width="" height="">
            <img id="" width="" height="" />
        </canvas>
    </div>

## Example ##

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>
        <div id="wrap">
            
            <div id="flipbook"></div>
    
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/flipbook.js"></script>
        <script>
            var urls = ['', '', '', ...]; //all paths of you images.
            var $flipbook = $('#flipbook').flipbook('init', {
                urls: urls, 
                autoplay: false, 
                backward: false,
                fps:30,
                repeat:false,

                //events
                onPreload: $.noop,
                onLoad: function(){
                    if($flipbook.isReady){
                        $player.play();
                    }
                } 
            });
        </script>
    </body>
    </html>
