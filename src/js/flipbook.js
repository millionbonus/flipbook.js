/*!
    flipbook.js v1.1.0.0
    flipbook.js is a jquery plugin which let you play a sequence of images as a small film on the fly.
    (c) 2014 Edison Chuang - https://bitbucket.org/millionbonus/flipbook.js/
    license: http://www.opensource.org/licenses/mit-license.php
*/

(function ($, window, undefined) {
    
    //fix ie8 leak of support of bind()
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5 internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }
            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () { },
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP && oThis
                                           ? this
                                           : oThis,
                                         aArgs.concat(Array.prototype.slice.call(arguments)));
                };
            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();
            return fBound;
        };
    }

    if (!Array.indexOf) {
        Array.prototype.indexOf = function (obj) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] == obj) {
                    return i;
                }
            }
            return -1;
        }
    }

    $.flipbook = function (el, options) {
        var flipbook          = this;
        flipbook.$el          = $(el);
        flipbook.el           = el;
        flipbook.settings     = $.extend(true, {}, $.fn.flipbook.defaultSettings, options);
        flipbook.frames       = [];
        flipbook.isReady      = false;
        flipbook.currentIndex = -1;
        flipbook.status       = 'stop';
        
        flipbook.play = function () {
            if (!this.isReady) {
                console.log('not ready');
                return;
            }
            var f = this;
            if (f.status == 'stop') {
                if(!f.settings.backward){
                    f.currentIndex = -1;
                }else{
                    f.currentIndex = f.frames.length;
                }
            } else if (f.status == 'play') {
                return;
            }
            f.status = 'play';
            
            var $canvas = f.$el.children('canvas').eq(0);
            var $img    = $canvas.children('img').eq(0);
            var _w      = $canvas.attr('width');
            var _h      = $canvas.attr('height');
            var ctx     = $.fn.flipbook.canvasSupported ? $canvas[0].getContext('2d') : undefined;
            
            (function drawFrame() {
                if (f.status == 'pause' || f.status == 'stop') {
                    return;
                }
                
                f.currentIndex = f.settings.backward ? (f.currentIndex - 1) : (f.currentIndex + 1);
                if (f.currentIndex >= 0 && f.currentIndex < f.frames.length) {
                    //not yet finish and request new frame
                    if ($.fn.flipbook.canvasSupported) {
                        ctx.clearRect(0, 0, _w, _h);
                        ctx.drawImage(f.frames[f.currentIndex], 0, 0, _w, _h);
                    } else {
                        $img.attr("src", f.frames[f.currentIndex].src);
                    }
                    
                    setTimeout(drawFrame, 1000 / f.settings.fps);
                    if ($.isFunction(f.settings.onFrame)) {
                        f.settings.onFrame.apply(f, [f.currentIndex]);
                    }
                } else {
                    //finish
                    f.stop.apply(f);
                    if ($.isFunction(f.settings.onFinish)) {
                        f.settings.onFinish.apply(f);
                    }
                    if(f.settings.repeat){
                        f.play.apply(f);
                    }
                }
            })();

        };
        
        flipbook.pause = function () {
            if (this.status == 'play') {
                this.status = 'pause';
            }
        };
        
        flipbook.stop = function () {
            this.status = 'stop';
        };

        flipbook.setCurrentIndex = function (index) {
            if (index < 0 || index > this.frames.length) {
                console.log('out of index');
                return;
            }
            this.status = 'pause';
            this.currentIndex = index;
            var $canvas = this.$el.children('canvas').eq(0);
            var $img = $canvas.children('img').eq(0);
            var _w = $canvas.attr('width');
            var _h = $canvas.attr('height');
            var ctx = $.fn.flipbook.canvasSupported ? $canvas[0].getContext('2d') : undefined;
            if ($.fn.flipbook.canvasSupported) {
                ctx.clearRect(0, 0, _w, _h);
                
                ctx.drawImage(this.frames[this.currentIndex], 0, 0, _w, _h);
            } else {
                $img.attr("src", this.frames[this.currentIndex].src);
            }
         
        };
        
        //init
        (function (flipbook) {
            //init structure
            var $aternative_img = $('<img>').css({ width: flipbook.settings.width, height: flipbook.settings.height });
            var $canvas = $('<canvas>').attr({width: flipbook.settings.width, height: flipbook.settings.height}).append($aternative_img);
            flipbook.$el.append($canvas);

            //#region preload images
            var urls = flipbook.settings.urls;
            var _preload = function (e) {
                var total = urls.length;
                var completed = 0;
                for (var i = 0; i < total; i++) {
                    var img = flipbook.frames[i];
                    if (img && img.complete && img.naturalWidth != 0) {
                        completed++;
                    }
                }
                if ($.isFunction(flipbook.settings.onPreload)) {
                    flipbook.settings.onPreload.apply(this, [e, total, completed]);
                }
                
                if (completed == total && !flipbook.isReady && $.isFunction(flipbook.settings.onload)) {
                    //only fire at first time
                    flipbook.isReady = true;
                    flipbook.settings.onload.apply(flipbook, [e]);
                    
                    if (flipbook.settings.autoplay) {
                        flipbook.play.apply(flipbook);
                    }
                }
            };
            
            for (var i = 0; i < urls.length; i++) {
                flipbook.frames[i] = new Image();
                flipbook.frames[i].onload = _preload.bind(flipbook.frames[i]);
                flipbook.frames[i].onerror = _preload.bind(flipbook.frames[i]);
                flipbook.frames[i].src = urls[i];
            }
            //#endregion
            
        })(flipbook);
    }



    $.fn.flipbook = function (options) {

        if (typeof (options) != 'string') {
            //init
            return this.each(function () {
                var flipbook = new $.flipbook(this, options);
                flipbook.$el.data('flipbook', flipbook);
            });

        } else {
            //method call
            return this.each(function () {
                var flipbook = $(this).data('flipbook');
                if (flipbook) {
                    switch (options) {
                        case 'play':
                            flipbook.play.apply(flipbook);
                            break;
                        default:
                            throw '"' + options +  '" was undefined in flipbook'
                    }
                }
            });
        }
    };

    $.fn.flipbook.defaultSettings = {
        width: 300,
        height: 300,
        urls: [],
        repeat: false,
        autoplay: false,
        backward: false,
        fps: 30,
        onPreload: $.noop,
        onLoad: $.noop,
        onFrame: $.noop,
        onFinish: $.noop
    };

    $.fn.flipbook.canvasSupported = document.createElement('canvas').getContext ? true : false;

}(jQuery, window, undefined));
